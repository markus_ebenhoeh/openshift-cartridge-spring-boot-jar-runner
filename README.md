# Spring-Boot-Jar-Runner
# OpenShift Cartridge to run binary JAR packaged Sping-Boot web applications


The first efforts will focus on the JAR part.
The main goal is that  - unlike a DIY - cartridge, the application using this cartridge have to be scalable. 

Binary deployments, preferable without git being involved are further goals.



## Template

This cartridge runs self-contained executable JAR applications. The cartridge can run any executable jar even when it has nothing to do with Spring-Boot.
To test the cartridge and to be able to deploy anything the template has a simple web server which is packaged as an executable JAR and returns some information about the OpenShift environment.
The code packaged as a template for the JAR is part of simple-executable-jar-web-server under https://bitbucket.org/markus_ebenhoeh/simple-executable-jar-web-server


## Cartridge Variables

The following variables are preset by the cartridge. To change the behaviour read on the [User Variables](#user-variables) as well as [_Overriding The Java Command Parameters_](#overriding-the-java_command-parameters)


### `OPENSHIFT_SBJR_JAVA_OPTS`
    
The `OPENSHIFT_SBJR_JAVA_OPTS` variable will be set to 

    -server \       
       -Dlogging.path=${OPENSHIFT_SBJR_LOG_DIR} \
       -Dspring.profiles.active=cloud

The value is reset for every start/stop as well as any other invocation. 

### `OPENSHIFT_SBJR_JAVA_OPTS_SERVER_NET`
    
The `OPENSHIFT_SBJR_JAVA_OPTS_SERVER_SOCKET_ADDRESS` variable will be set to 

    -Dserver.address=${OPENSHIFT_SBJR_IP} -Dserver.port=${OPENSHIFT_SBJR_HTTP_PORT}

The gear will populate the IP (`OPENSHIFT_SBJR_IP`) and port (`OPENSHIFT_SBJR_HTTP_PORT`) variables and expects the server to run on that socket address (address:port); otherwise the routing will not work and the application will not be available.

The value is reset for every start/stop as well as any other invocation.

## User Variables

The following variables  can be used to  change behaviour. This have to be set for the application.

See _Overriding The Java Command Parameters_](#overriding-the-java_command-parameters) for how this works for this cartridge.

See [A blog entry][2] on how to set application environment variables.
     
     rhc env set USER_VAR='something' --app myapplication

I believe there should be a way to set the environment variables with files as part of the [".openshift" directory][1] of an application but have not found any references.
This might have to be done by the cartridge.


### `OPENSHIFT_SBJR_JAVA_OPTS_OVERRIDE`
    
When `OPENSHIFT_SBJR_JAVA_OPTS_OVERRIDE` is set to any (non-empty) value, then the cartridge will set the cartridge's internal variable `OPENSHIFT_SBJR_JAVA_OPTS` to the content `OPENSHIFT_SBJR_JAVA_OPTS_OVERRIDE`.

### `OPENSHIFT_SBJR_JAVA_OPTS_SERVER_NET_OVERRIDE`
    
When `OPENSHIFT_SBJR_JAVA_OPTS_SERVER_NET_OVERRIDE` is set to any (non-empty) value, then the cartridge will set the cartridge's internal variable
`OPENSHIFT_SBJR_JAVA_OPTS_SERVER_NET` to the content of `OPENSHIFT_SBJR_JAVA_OPTS_SERVER_NET_OVERRIDE`

In that case the application developers have to set `-Dserver.address=${OPENSHIFT_SBJR_IP}` and `-Dserver.port=${OPENSHIFT_SBJR_HTTP_PORT}` themselves, by either passing it in  `OPENSHIFT_SBJR_JAVA_OPTS_SERVER_NET_OVERRIDE` or in `OPENSHIFT_SBJR_JAVA_OPTS`.


### `OPENSHIFT_SBJR_JAVA_OPTS_CUSTOM`

The optional variable `OPENSHIFT_SBJR_JAVA_OPTS_CUSTOM`  is appended to the java command line right after  `OPENSHIFT_SBJR_JAVA_OPTS`.



## Overriding The Java Command Parameters


The java command is executed thusly:

        java ${OPENSHIFT_SBJR_JAVA_OPTS_SERVER_NET} \
             ${OPENSHIFT_SBJR_JAVA_OPTS} \
             ${OPENSHIFT_SBJR_JAVA_OPTS_CUSTOM} \
             -jar ${jarFile} 1>${OPENSHIFT_SBJR_LOG_DIR}/java.out 2>${OPENSHIFT_SBJR_LOG_DIR}/java.err.out &

To override specific settings e.g. the active profile, use `OPENSHIFT_SBJR_JAVA_OPTS_CUSTOM` which is appended after OPENSHIFT_SBJR_JAVA_OPTS. so e.g. setting

    OPENSHIFT_SBJR_JAVA_OPTS_CUSTOM=-Dspring.profiles.active=default

will override the value set in `OPENSHIFT_SBJR_JAVA_OPT` and will lead to the system property spring.profiles.active being set to default instead of cloud.

If overriding individual settings is not an option, then `OPENSHIFT_SBJR_JAVA_OPTS_OVERRIDE` can be used, which will be used to set `OPENSHIFT_SBJR_JAVA_OPTS`.

The same applies to `OPENSHIFT_SBJR_JAVA_OPTS_SERVER_NET_OVERRIDE`, which overrides `OPENSHIFT_SBJR_JAVA_OPTS_SERVER_NET`   

[1]: http://openshift.github.io/documentation/oo_user_guide.html#the-openshift-directory 
[2]: https://www.openshift.com/blogs/new-online-features-for-september-2013
    