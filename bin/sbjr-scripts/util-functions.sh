#!/bin/bash 

function update_configuration {
    export_java_home
    reinstall_path
}

function export_java_home() {
  if marker_present "java6"; then
    export JAVA_HOME=$OPENSHIFT_SBJR_JDK6
  else
    export JAVA_HOME=$OPENSHIFT_SBJR_JDK7
  fi
}

function reinstall_path {
  echo $JAVA_HOME > $OPENSHIFT_SBJR_DIR/env/JAVA_HOME
  echo "$JAVA_HOME/bin" > $OPENSHIFT_SBJR_DIR/env/OPENSHIFT_SBJR_PATH_ELEMENT
}

function cart-log {
  echo  "$( date "+%Y%m%d_%H%M%S" ) $@ " >>  ${OPENSHIFT_SBJR_LOG_DIR}/cart-SBJR-log.out
}

function cart-log-message {
  echo  "$( date "+%Y%m%d_%H%M%S" ) $@ " >>  ${OPENSHIFT_SBJR_LOG_DIR}/cart-SBJR-log.out
  client_message  "$( date "+%Y%m%d_%H%M%S" ) $@ "
}


function cart-log-error {
  echo "$( date "+%Y%m%d_%H%M%S" ) ERROR $@ " >>  ${OPENSHIFT_SBJR_LOG_DIR}/cart-SBJR-log.out
  client_error "$( date "+%Y%m%d_%H%M%S" ) $@ "
  echo "$( date "+%Y%m%d_%H%M%S" ) ERROR $@ " 1>&2
}

function rotate_logs() {
   mv ${OPENSHIFT_SBJR_LOG_DIR}/java.out ${OPENSHIFT_SBJR_LOG_DIR}/java.out.$( date "+%Y%m%d_%H%M%S" )
   mv ${OPENSHIFT_SBJR_LOG_DIR}/java.err.out ${OPENSHIFT_SBJR_LOG_DIR}/java.err.out$( date "+%Y%m%d_%H%M%S" )
}

